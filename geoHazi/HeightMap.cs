﻿using System;
using System.Text;
using System.IO;
using System.Drawing;

namespace geoHazi
{
    class HeightMap
    {
        public static int size = 1201;
        public int[,] map; 
        public int[] MinAndMax { get; set; }
        public int Heights { get; set; }
        public int ElevationThreshold { get; set; }

        public HeightMap() {
            this.map = new int[size,size];
        }

        public static HeightMap Parse(string path) {
            HeightMap mp = new HeightMap();
            using (FileStream fs2 = new FileStream(path, FileMode.Open))
            {
                using (BinaryReader br2 = new BinaryReader(fs2))
                {
                    int[] tmp = new int[2];
                    for (int i = 0; i < size; ++i)
                    {
                        for (int j = 0; j < size; j++)
                        {
                            tmp[0] = br2.ReadByte();
                            tmp[1] = br2.ReadByte();
                            mp.map[i, j] = tmp[0] << 8 | tmp[1]; 
                        }
                    }
                }
            }

            return mp;
        }
        private int[] GetExtremalElevations() {
            int min = int.MaxValue;
            int max = int.MinValue;
            for (int i = 0; i < ElevationThreshold; i++)
            {
                for (int j = 0; j < ElevationThreshold; j++)
                {
                    if (map[i,j] < min) {
                        min = map[i, j];
                    }
                    if (map[i, j] > max) {
                        max = map[i, j];
                    }
                }
            }
            return new int[] { min, max };
        }
        public void SaveToBitmap(string path){
            Bitmap bmp = new Bitmap(size, size);
            Color myColor = Color.FromArgb(0, 0, 0);

            int [] perimiters = this.GetExtremalElevations();

            int devide = 1;
            devide = (perimiters[1] - perimiters[0]) / 255;

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    if (map[i, j] == 32768) {
                        myColor = Color.FromArgb(255, 255, 255);
                    } 
                    if (map[i, j] > this.ElevationThreshold)
                    {
                        myColor = Color.FromArgb(0, Math.Abs((int)(255 - (map[i, j] ))/255), 0);
                    }
                    else
                        myColor = Color.FromArgb(0, 0, 255);

                    bmp.SetPixel(j, i, myColor);
                }

            }
            bmp.Save(path);
        }
    }
}
